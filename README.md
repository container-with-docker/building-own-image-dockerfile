# Building own image-Dockerfile

## Containerizing Application

This technology has change the way software development was done for the better. In essence it makes application portable and compatible, to easily run in different platforms with no configuration apart from installing a container runtime on the platforms. 
Traditionally, before the advent of container technology, if one has to move an application from one environemnt to another, one will need to installs and configure the application depencies like and binaries on the new environment before executing the application.

The workflow:

- start with a base image the application is using as a depency.
  In this case, the application is using Nodejs for backend, so 
  From node, will be the first call in the Dockerfile
- The next will be to set environemntal variables like if the 
  application will have to connect to a DB, one has to set
  set variables that call the passwords and username.
- The next step will be to create a directory inside the contain with the run
   command.
   followed by copying the source code and it dependencies files into the newly
   created directory.
- The last step CMD[] will instruct how the application inside the container should be started.



```
Technology Used
Docker, Nodejs, AWS ECR(Elastic Container Registry)


```

```
cd existing_repo
git remote add origin https://gitlab.com/container-with-docker/building-own-image-dockerfile.git
git branch -M main
git push -uf origin main
```

